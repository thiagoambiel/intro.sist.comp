# Atividades de "Introdução a Sistemas Computacionais"

Esta página mantém todas as atividades passadas em aula e o trabalho final da matéria **Introdução a Sistemas Computacionais** ministrada pelo professor Eduardo do Valle Simões.

# Portas Lógicas

O diretório [**portas_logicas**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/tree/main/portas_logicas) possui os esquemas das portas logicas **NOT**, **AND**, **OR** e **XOR** criados através do simulador de circuitos [Falstad](https://www.falstad.com/circuit/).

# Algoritmos

O diretório [**algoritmos**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/tree/main/algoritmos) possui os algoritmos de multiplicação e divisão de números inteiros que são implementados em microprocessadores reais. Além de uma máquina de estados que simula um sistema de alarme de carro.

# Simulador

O diretório [**simulador**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/tree/main/simulador) possui o código fonte completado do `simple_simulator.c`, uma versão incompleta do Simulador ICMC.

# Caderno Digital

O diretório [**caderno**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/tree/main/caderno) possui imagens das atividades que foram passadas em sala para serem realizadas no caderno.


# Jogo

O diretório [**jogo**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/tree/main/jogo) possui o código fonte do trabalho final da matéria (Criar um jogo compatível com o Processador ICMC) junto de um tutorial de como instalar e executar o simulador do processador e o próprio jogo.
