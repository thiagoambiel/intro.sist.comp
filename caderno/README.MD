# Caderno Digital

Aqui se encontram as atividades que foram passadas em aula para serem realizadas no caderno. 

## Clique no nome de um atividade para mostrar o conteúdo.

<details><summary><b>Circulo do Complemento de Dois</b></summary>

<img src="https://gitlab.com/thiagoambiel/intro.sist.comp/-/raw/main/caderno/complemento_de_2.png" width="450" height="410" />

</details>


<details><summary><b>ULA que Soma</b></summary>

<img src="https://gitlab.com/thiagoambiel/intro.sist.comp/-/raw/main/caderno/ula_soma.png" width="410" height="450" />

</details>

<details><summary><b>ULA que Subtrai</b></summary>

<img src="https://gitlab.com/thiagoambiel/intro.sist.comp/-/raw/main/caderno/ula_subtrai.png" width="410" height="450" />

</details>

<details><summary><b>Multiplexador do Celular (64 bits)</b></summary>

<img src="https://gitlab.com/thiagoambiel/intro.sist.comp/-/raw/main/caderno/mux.png" width="605" height="443" />

</details>
