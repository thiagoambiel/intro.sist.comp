;---------- BEGIN - PRE PROCESSAMENTO ----------
; variavel: cor_branca alocada em 28000 com 1 bytes

; endereco temporario(27999) recebe o conteudo: 0
loadn r0, #0
store 27999, r0

load r0, 27999
store 28000, r0
; variavel: cor_oliva alocada em 27999 com 1 bytes

; endereco temporario(27998) recebe o conteudo: 768
loadn r0, #768
store 27998, r0

load r0, 27998
store 27999, r0
; variavel: cor_marrom alocada em 27998 com 1 bytes

; endereco temporario(27997) recebe o conteudo: 255
loadn r0, #255
store 27997, r0

load r0, 27997
store 27998, r0
; variavel: cor_roxo alocada em 27997 com 1 bytes

; endereco temporario(27996) recebe o conteudo: 1280
loadn r0, #1280
store 27996, r0

load r0, 27996
store 27997, r0
; variavel: cor_verde alocada em 27996 com 1 bytes

; endereco temporario(27995) recebe o conteudo: 512
loadn r0, #512
store 27995, r0

load r0, 27995
store 27996, r0
; variavel: cor_amarela alocada em 27995 com 1 bytes

; endereco temporario(27994) recebe o conteudo: 2816
loadn r0, #2816
store 27994, r0

load r0, 27994
store 27995, r0
; variavel: cor_azul alocada em 27994 com 1 bytes

; endereco temporario(27993) recebe o conteudo: 3072
loadn r0, #3072
store 27993, r0

load r0, 27993
store 27994, r0
; variavel: cor_vermelho alocada em 27993 com 1 bytes

; endereco temporario(27992) recebe o conteudo: 2304
loadn r0, #2304
store 27992, r0

load r0, 27992
store 27993, r0
; variavel: cor_cinza alocada em 27992 com 1 bytes

; endereco temporario(27991) recebe o conteudo: 2048
loadn r0, #2048
store 27991, r0

load r0, 27991
store 27992, r0
; variavel: tabua_esquerda alocada em 27991 com 1 bytes

; endereco temporario(27990) recebe o conteudo: 3
loadn r0, #3
store 27990, r0

load r0, 27990
store 27991, r0
; variavel: tabua_direita alocada em 27990 com 1 bytes

; endereco temporario(27989) recebe o conteudo: 2
loadn r0, #2
store 27989, r0

load r0, 27989
store 27990, r0
; variavel: bloco alocada em 27989 com 1 bytes

; endereco temporario(27988) recebe o conteudo: 1
loadn r0, #1
store 27988, r0

load r0, 27988
store 27989, r0
; variavel: bola alocada em 27988 com 1 bytes

; endereco temporario(27987) recebe o conteudo: 0
loadn r0, #0
store 27987, r0

load r0, 27987
store 27988, r0
; variavel: pos_x alocada em 27987 com 1 bytes

; endereco temporario(27986) recebe o conteudo: 1
loadn r0, #1
store 27986, r0

load r0, 27986
store 27987, r0
; variavel: bola_delay alocada em 27986 com 1 bytes

; endereco temporario(27985) recebe o conteudo: 10
loadn r0, #10
store 27985, r0

load r0, 27985
store 27986, r0
; variavel: bola_x alocada em 27985 com 1 bytes

; endereco temporario(27984) recebe o conteudo: 10
loadn r0, #10
store 27984, r0

load r0, 27984
store 27985, r0
; variavel: bola_y alocada em 27984 com 1 bytes

; endereco temporario(27983) recebe o conteudo: 20
loadn r0, #20
store 27983, r0

load r0, 27983
store 27984, r0
; variavel: bola_direcao alocada em 27983 com 1 bytes

; endereco temporario(27982) recebe o conteudo: 0
loadn r0, #0
store 27982, r0

load r0, 27982
store 27983, r0
; @(101) INSTRUCAO - while
;---------- END PRE PROCESSAMENTO ----------
;---------- BEGIN ----------
; variavel: cor_branca alocada em 28000

; variavel: cor_oliva alocada em 27999

; variavel: cor_marrom alocada em 27998

; variavel: cor_roxo alocada em 27997

; variavel: cor_verde alocada em 27996

; variavel: cor_amarela alocada em 27995

; variavel: cor_azul alocada em 27994

; variavel: cor_vermelho alocada em 27993

; variavel: cor_cinza alocada em 27992

; variavel: tabua_esquerda alocada em 27991

; variavel: tabua_direita alocada em 27990

; variavel: bloco alocada em 27989

; variavel: bola alocada em 27988

; variavel: pos_x alocada em 27987

; variavel: bola_delay alocada em 27986

; variavel: bola_x alocada em 27985

; variavel: bola_y alocada em 27984

; variavel: bola_direcao alocada em 27983

jmp __function_label_main__
__printf__:
loadn r6, #0
__printf_loop__:
loadn r4, #'%'
loadi r1, r2
cmp r1, r4
jeq __side_printf__
cmp r1, r6
jeq __printf_fim__
outchar r1, r0
inc r0
dec r2
jmp __printf_loop__
__printf_fim__:
rts
__side_printf__:
dec r2
loadi r5, r2
loadn r4, #'d'
cmp r5, r4
jeq __printf_d__
loadn r4, #'s'
cmp r5, r4
jeq __printf_s__
loadn r4, #'S'
cmp r5, r4
jeq __printf_S__
loadn r4, #'c'
cmp r5, r4
jeq __printf_c__
outchar r1, r0
inc r0
outchar r5, r0
inc r0
jmp __printf_loop__
__printf_d__:
push r1
loadn r1, #0
dec r2
loadi r5, r2
loadi r5, r5
dec r2
loadn r7, #10
__printf_d_loop__:
div r4, r5, r7
mul r3, r4, r7
sub r3, r5, r3
push r3
inc r1
cmp r4, r6
jeq __printf_d_imprime__
mov r5, r4
jmp __printf_d_loop__
__printf_d_imprime__:
loadn r7, #'0'
pop r4
add r4, r4, r7
outchar r4, r0
inc r0
dec r1
cmp r1, r6
jne __printf_d_imprime__
pop r1
jmp __printf_loop__
__printf_s__:
dec r2
loadi r4, r2
loadi r4, r4
dec r2
__side_printf_volta__:
loadi r5, r4
cmp r5, r6
jeq __printf_loop__
outchar r5, r0
dec r4
inc r0
jmp __side_printf_volta__
__printf_S__:
dec r2
loadi r4, r2
dec r2
__side_printf2_volta__:
loadi r5, r4
cmp r5, r6
jeq __printf_loop__
outchar r5, r0
dec r4
inc r0
jmp __side_printf2_volta__
__printf_c__:
dec r2
loadi r5, r2
loadi r5, r5
outchar r5, r0
dec r2
inc r0
jmp __printf_loop__
__and_routine__:
and r0, r1, r0
loadn r1, #0
cmp r0, r1
jne __true__
jmp __false__
__or_routine__:
or r0, r1, r0
loadn r1, #0
cmp r0, r1
jne __true__
jmp __false__
__equal_routine__:
cmp r0, r1
jne __false__
jmp __true__
__not_equal_routine__:
cmp r0, r1
jne __true__
jmp __false__
__less_routine__:
cmp r0, r1
jle __true__
jmp __false__
__more_routine__:
cmp r0, r1
jgr __true__
jmp __false__
__equal_less_routine__:
cmp r0, r1
jel __true__
jmp __false__
__equal_more_routine__:
cmp r0, r1
jeg __true__
jmp __false__
__true__:
loadn r0, #1
storei r2, r0
rts
__false__:
loadn r0, #0
storei r2, r0
rts
; @(47) INSTRUCAO - ; declarando funcao: main
__function_label_main__:

; variavel: i alocada em 27982 com 1 bytes

; variavel: j alocada em 27981 com 1 bytes

; variavel: mapa alocada em 27980 com 931 bytes

; @(50) INSTRUCAO - acerta o ponteiro da matriz/vetor
loadn r0, #27979
store 27980, r0
loadn r0, #27979
loadn r1, #27949
storei r0, r1
dec r0
loadn r1, #27919
storei r0, r1
dec r0
loadn r1, #27889
storei r0, r1
dec r0
loadn r1, #27859
storei r0, r1
dec r0
loadn r1, #27829
storei r0, r1
dec r0
loadn r1, #27799
storei r0, r1
dec r0
loadn r1, #27769
storei r0, r1
dec r0
loadn r1, #27739
storei r0, r1
dec r0
loadn r1, #27709
storei r0, r1
dec r0
loadn r1, #27679
storei r0, r1
dec r0
loadn r1, #27649
storei r0, r1
dec r0
loadn r1, #27619
storei r0, r1
dec r0
loadn r1, #27589
storei r0, r1
dec r0
loadn r1, #27559
storei r0, r1
dec r0
loadn r1, #27529
storei r0, r1
dec r0
loadn r1, #27499
storei r0, r1
dec r0
loadn r1, #27469
storei r0, r1
dec r0
loadn r1, #27439
storei r0, r1
dec r0
loadn r1, #27409
storei r0, r1
dec r0
loadn r1, #27379
storei r0, r1
dec r0
loadn r1, #27349
storei r0, r1
dec r0
loadn r1, #27319
storei r0, r1
dec r0
loadn r1, #27289
storei r0, r1
dec r0
loadn r1, #27259
storei r0, r1
dec r0
loadn r1, #27229
storei r0, r1
dec r0
loadn r1, #27199
storei r0, r1
dec r0
loadn r1, #27169
storei r0, r1
dec r0
loadn r1, #27139
storei r0, r1
dec r0
loadn r1, #27109
storei r0, r1
dec r0
loadn r1, #27079
storei r0, r1
dec r0

; @(50) INSTRUCAO - acerta o ponteiro da matriz/vetor
;----- begin for -----
; endereco temporario(27049) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27049, r0

; endereco temporario(27048) recebe o conteudo: 0
loadn r0, #0
store 27048, r0

; assignment salvando no endereco apontado por: endereco temporario(27049) o conteudo de endereco temporario(27048)
load r0, 27048
; valor da stack: 27047
store 27982, r0 ; variavel i recebe o conteudo de r0

__loop_label_begin8__:
; endereco temporario(27049) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27049, r0

; endereco temporario(27048) recebe o conteudo: 30
loadn r0, #30
store 27048, r0

; @(52) INSTRUCAO - relacao_binaria
load r0, 27049
load r1, 27048
loadn r2, #27049
call __less_routine__
load r0, 27049
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label8__
jmp __loop_label3_begin8__
__loop_label2_begin8__:
; endereco temporario(27049) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27049, r0

load r0, 27049
inc r0
store 27049, r0
store 27982, r0
jmp __loop_label_begin8__
__loop_label3_begin8__:
;----- begin for -----
; endereco temporario(27048) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27048, r0

; endereco temporario(27047) recebe o conteudo: 0
loadn r0, #0
store 27047, r0

; assignment salvando no endereco apontado por: endereco temporario(27048) o conteudo de endereco temporario(27047)
load r0, 27047
; valor da stack: 27046
store 27981, r0 ; variavel j recebe o conteudo de r0

__loop_label_begin9__:
; endereco temporario(27048) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27048, r0

; endereco temporario(27047) recebe o conteudo: 30
loadn r0, #30
store 27047, r0

; @(53) INSTRUCAO - relacao_binaria
load r0, 27048
load r1, 27047
loadn r2, #27048
call __less_routine__
load r0, 27048
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label9__
jmp __loop_label3_begin9__
__loop_label2_begin9__:
; endereco temporario(27048) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27048, r0

load r0, 27048
inc r0
store 27048, r0
store 27981, r0
jmp __loop_label_begin9__
__loop_label3_begin9__:
; endereco temporario(27047) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27047, r0

; endereco temporario(27046) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27046, r0

; endereco temporario(27045) recebe o conteudo da variavel: mapa
load r0, 27046
load r1, 27047
load r2, 27980
sub r2, r2, r1
loadi r1, r2
sub r7, r1, r0
loadi r1, r7
store 27047, r1
; matriz, valor da stack 27047
; endereco temporario(27046) recebe o conteudo: ' '
loadn r0, #' '
store 27046, r0

; assignment salvando no endereco apontado por: endereco temporario(27047) o conteudo de endereco temporario(27046)
load r0, 27046
; valor da stack: 27045
storei r7, r0 ; endereco apontado por r7 recebe o conteudo de r0

jmp __loop_label2_begin9__
__exit_loop_label9__:
; ----- end for -----
jmp __loop_label2_begin8__
__exit_loop_label8__:
; ----- end for -----
;----- begin for -----
; endereco temporario(27047) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27047, r0

; endereco temporario(27046) recebe o conteudo: 2
loadn r0, #2
store 27046, r0

; assignment salvando no endereco apontado por: endereco temporario(27047) o conteudo de endereco temporario(27046)
load r0, 27046
; valor da stack: 27045
store 27982, r0 ; variavel i recebe o conteudo de r0

__loop_label_begin10__:
; endereco temporario(27047) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27047, r0

; endereco temporario(27046) recebe o conteudo: 30
loadn r0, #30
store 27046, r0

; @(58) INSTRUCAO - relacao_binaria
load r0, 27047
load r1, 27046
loadn r2, #27047
call __less_routine__
load r0, 27047
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label10__
jmp __loop_label3_begin10__
__loop_label2_begin10__:
; endereco temporario(27047) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27047, r0

load r0, 27047
inc r0
store 27047, r0
store 27982, r0
jmp __loop_label_begin10__
__loop_label3_begin10__:
;----- begin for -----
; endereco temporario(27046) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27046, r0

; endereco temporario(27045) recebe o conteudo: 0
loadn r0, #0
store 27045, r0

; assignment salvando no endereco apontado por: endereco temporario(27046) o conteudo de endereco temporario(27045)
load r0, 27045
; valor da stack: 27044
store 27981, r0 ; variavel j recebe o conteudo de r0

__loop_label_begin11__:
; endereco temporario(27046) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27046, r0

; endereco temporario(27045) recebe o conteudo: 30
loadn r0, #30
store 27045, r0

; @(59) INSTRUCAO - relacao_binaria
load r0, 27046
load r1, 27045
loadn r2, #27046
call __less_routine__
load r0, 27046
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label11__
jmp __loop_label3_begin11__
__loop_label2_begin11__:
; endereco temporario(27046) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27046, r0

load r0, 27046
inc r0
store 27046, r0
store 27981, r0
jmp __loop_label_begin11__
__loop_label3_begin11__:
; endereco temporario(27045) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27045, r0

; endereco temporario(27044) recebe o conteudo: 2
loadn r0, #2
store 27044, r0

; @(60) INSTRUCAO - relacao_binaria
load r0, 27045
load r1, 27044
loadn r2, #27045
call __equal_routine__
; endereco temporario(27044) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27044, r0

; endereco temporario(27043) recebe o conteudo: 29
loadn r0, #29
store 27043, r0

; @(60) INSTRUCAO - relacao_binaria
load r0, 27044
load r1, 27043
loadn r2, #27044
call __equal_routine__
; @(60) INSTRUCAO - relacao_binaria
load r0, 27045
load r1, 27044
loadn r2, #27045
call __or_routine__
; endereco temporario(27044) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27044, r0

; endereco temporario(27043) recebe o conteudo: 0
loadn r0, #0
store 27043, r0

; @(60) INSTRUCAO - relacao_binaria
load r0, 27044
load r1, 27043
loadn r2, #27044
call __equal_routine__
; @(60) INSTRUCAO - relacao_binaria
load r0, 27045
load r1, 27044
loadn r2, #27045
call __or_routine__
; endereco temporario(27044) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27044, r0

; endereco temporario(27043) recebe o conteudo: 29
loadn r0, #29
store 27043, r0

; @(60) INSTRUCAO - relacao_binaria
load r0, 27044
load r1, 27043
loadn r2, #27044
call __equal_routine__
; @(60) INSTRUCAO - relacao_binaria
load r0, 27045
load r1, 27044
loadn r2, #27045
call __or_routine__
load r0, 27045
loadn r1, #0
cmp r0, r1
jeq __exit_if_label1__
jmp __if_label1__
__if_label1__:
; endereco temporario(27045) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27045, r0

; endereco temporario(27044) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27044, r0

; endereco temporario(27043) recebe o conteudo da variavel: mapa
load r0, 27044
load r1, 27045
load r2, 27980
sub r2, r2, r1
loadi r1, r2
sub r7, r1, r0
loadi r1, r7
store 27045, r1
; matriz, valor da stack 27045
; endereco temporario(27044) recebe o conteudo da variavel: bloco(27989)
load r0, 27989
store 27044, r0

; assignment salvando no endereco apontado por: endereco temporario(27045) o conteudo de endereco temporario(27044)
load r0, 27044
; valor da stack: 27043
storei r7, r0 ; endereco apontado por r7 recebe o conteudo de r0

; endereco temporario(27045) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27045, r0

; endereco temporario(27044) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27044, r0

; endereco temporario(27043) recebe o conteudo da variavel: bloco(27989)
load r0, 27989
store 27043, r0

; endereco temporario(27042) recebe o conteudo da variavel: cor_cinza(27992)
load r0, 27992
store 27042, r0

; expressao_binaria: 27043 + 27042
load r0, 27043
load r1, 27042
add r2, r0, r1
store 27043, r2

; @(62) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 27042, r0
loadn r0, #'c'
store 27041, r0
loadn r0, #27043
store 27040, r0
loadn r0, #0
store 27039, r0
load r0, 27045
load r1, 27044
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #27042
call __printf__
__exit_if_label1__:
jmp __loop_label2_begin11__
__exit_loop_label11__:
; ----- end for -----
jmp __loop_label2_begin10__
__exit_loop_label10__:
; ----- end for -----
;----- begin for -----
; endereco temporario(27038) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27038, r0

; endereco temporario(27037) recebe o conteudo: 8
loadn r0, #8
store 27037, r0

; assignment salvando no endereco apontado por: endereco temporario(27038) o conteudo de endereco temporario(27037)
load r0, 27037
; valor da stack: 27036
store 27982, r0 ; variavel i recebe o conteudo de r0

__loop_label_begin12__:
; endereco temporario(27038) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27038, r0

; endereco temporario(27037) recebe o conteudo: 12
loadn r0, #12
store 27037, r0

; @(67) INSTRUCAO - relacao_binaria
load r0, 27038
load r1, 27037
loadn r2, #27038
call __less_routine__
load r0, 27038
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label12__
jmp __loop_label3_begin12__
__loop_label2_begin12__:
; endereco temporario(27038) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27038, r0

load r0, 27038
inc r0
store 27038, r0
store 27982, r0
jmp __loop_label_begin12__
__loop_label3_begin12__:
;----- begin for -----
; endereco temporario(27037) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27037, r0

; endereco temporario(27036) recebe o conteudo: 2
loadn r0, #2
store 27036, r0

; assignment salvando no endereco apontado por: endereco temporario(27037) o conteudo de endereco temporario(27036)
load r0, 27036
; valor da stack: 27035
store 27981, r0 ; variavel j recebe o conteudo de r0

__loop_label_begin13__:
; endereco temporario(27037) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27037, r0

; endereco temporario(27036) recebe o conteudo: 28
loadn r0, #28
store 27036, r0

; @(68) INSTRUCAO - relacao_binaria
load r0, 27037
load r1, 27036
loadn r2, #27037
call __less_routine__
load r0, 27037
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label13__
jmp __loop_label3_begin13__
__loop_label2_begin13__:
; endereco temporario(27037) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27037, r0

load r0, 27037
inc r0
store 27037, r0
store 27981, r0
jmp __loop_label_begin13__
__loop_label3_begin13__:
; variavel: cor alocada em 27036 com 1 bytes

; endereco temporario(27035) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27035, r0

; endereco temporario(27034) recebe o conteudo: 8
loadn r0, #8
store 27034, r0

; @(71) INSTRUCAO - relacao_binaria
load r0, 27035
load r1, 27034
loadn r2, #27035
call __equal_routine__
load r0, 27035
loadn r1, #0
cmp r0, r1
jeq __exit_if_label2__
jmp __if_label2__
__if_label2__:
; endereco temporario(27035) recebe o conteudo da variavel: cor(27036)
load r0, 27036
store 27035, r0

; endereco temporario(27034) recebe o conteudo da variavel: cor_azul(27994)
load r0, 27994
store 27034, r0

; assignment salvando no endereco apontado por: endereco temporario(27035) o conteudo de endereco temporario(27034)
load r0, 27034
; valor da stack: 27033
store 27036, r0 ; variavel cor recebe o conteudo de r0

__exit_if_label2__:
; endereco temporario(27035) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27035, r0

; endereco temporario(27034) recebe o conteudo: 9
loadn r0, #9
store 27034, r0

; @(75) INSTRUCAO - relacao_binaria
load r0, 27035
load r1, 27034
loadn r2, #27035
call __equal_routine__
load r0, 27035
loadn r1, #0
cmp r0, r1
jeq __exit_if_label3__
jmp __if_label3__
__if_label3__:
; endereco temporario(27035) recebe o conteudo da variavel: cor(27036)
load r0, 27036
store 27035, r0

; endereco temporario(27034) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 27034, r0

; assignment salvando no endereco apontado por: endereco temporario(27035) o conteudo de endereco temporario(27034)
load r0, 27034
; valor da stack: 27033
store 27036, r0 ; variavel cor recebe o conteudo de r0

__exit_if_label3__:
; endereco temporario(27035) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27035, r0

; endereco temporario(27034) recebe o conteudo: 10
loadn r0, #10
store 27034, r0

; @(79) INSTRUCAO - relacao_binaria
load r0, 27035
load r1, 27034
loadn r2, #27035
call __equal_routine__
load r0, 27035
loadn r1, #0
cmp r0, r1
jeq __exit_if_label4__
jmp __if_label4__
__if_label4__:
; endereco temporario(27035) recebe o conteudo da variavel: cor(27036)
load r0, 27036
store 27035, r0

; endereco temporario(27034) recebe o conteudo da variavel: cor_verde(27996)
load r0, 27996
store 27034, r0

; assignment salvando no endereco apontado por: endereco temporario(27035) o conteudo de endereco temporario(27034)
load r0, 27034
; valor da stack: 27033
store 27036, r0 ; variavel cor recebe o conteudo de r0

__exit_if_label4__:
; endereco temporario(27035) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27035, r0

; endereco temporario(27034) recebe o conteudo: 11
loadn r0, #11
store 27034, r0

; @(83) INSTRUCAO - relacao_binaria
load r0, 27035
load r1, 27034
loadn r2, #27035
call __equal_routine__
load r0, 27035
loadn r1, #0
cmp r0, r1
jeq __exit_if_label5__
jmp __if_label5__
__if_label5__:
; endereco temporario(27035) recebe o conteudo da variavel: cor(27036)
load r0, 27036
store 27035, r0

; endereco temporario(27034) recebe o conteudo da variavel: cor_amarela(27995)
load r0, 27995
store 27034, r0

; assignment salvando no endereco apontado por: endereco temporario(27035) o conteudo de endereco temporario(27034)
load r0, 27034
; valor da stack: 27033
store 27036, r0 ; variavel cor recebe o conteudo de r0

__exit_if_label5__:
; endereco temporario(27035) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27035, r0

; endereco temporario(27034) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27034, r0

; endereco temporario(27033) recebe o conteudo da variavel: mapa
load r0, 27034
load r1, 27035
load r2, 27980
sub r2, r2, r1
loadi r1, r2
sub r7, r1, r0
loadi r1, r7
store 27035, r1
; matriz, valor da stack 27035
; endereco temporario(27034) recebe o conteudo da variavel: bloco(27989)
load r0, 27989
store 27034, r0

; assignment salvando no endereco apontado por: endereco temporario(27035) o conteudo de endereco temporario(27034)
load r0, 27034
; valor da stack: 27033
storei r7, r0 ; endereco apontado por r7 recebe o conteudo de r0

; endereco temporario(27035) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 27035, r0

; endereco temporario(27034) recebe o conteudo da variavel: j(27981)
load r0, 27981
store 27034, r0

; endereco temporario(27033) recebe o conteudo da variavel: bloco(27989)
load r0, 27989
store 27033, r0

; endereco temporario(27032) recebe o conteudo da variavel: cor(27036)
load r0, 27036
store 27032, r0

; expressao_binaria: 27033 + 27032
load r0, 27033
load r1, 27032
add r2, r0, r1
store 27033, r2

; @(88) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 27032, r0
loadn r0, #'c'
store 27031, r0
loadn r0, #27033
store 27030, r0
loadn r0, #0
store 27029, r0
load r0, 27035
load r1, 27034
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #27032
call __printf__
jmp __loop_label2_begin13__
__exit_loop_label13__:
; ----- end for -----
jmp __loop_label2_begin12__
__exit_loop_label12__:
; ----- end for -----
; variavel: score alocada em 27028 com 1 bytes

; endereco temporario(27027) recebe o conteudo: 0
loadn r0, #0
store 27027, r0

load r0, 27027
store 27028, r0
; endereco temporario(27027) recebe o conteudo: 0
loadn r0, #0
store 27027, r0

; endereco temporario(27026) recebe o conteudo: 1
loadn r0, #1
store 27026, r0

; endereco temporario(27025) recebe o conteudo da variavel: score(27028)
load r0, 27028
store 27025, r0

; @(93) INSTRUCAO - ; printf(Score: %d);
loadn r0, #'S'
store 27024, r0
loadn r0, #'c'
store 27023, r0
loadn r0, #'o'
store 27022, r0
loadn r0, #'r'
store 27021, r0
loadn r0, #'e'
store 27020, r0
loadn r0, #':'
store 27019, r0
loadn r0, #' '
store 27018, r0
loadn r0, #'%'
store 27017, r0
loadn r0, #'d'
store 27016, r0
;comeco da string: 27024
;printf_args.size(): 1
; 27025
;j: 0
loadn r0, #27025
store 27015, r0
loadn r0, #0
store 27014, r0
load r0, 27027
load r1, 27026
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #27024
call __printf__
; endereco temporario(27013) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 27013, r0

; endereco temporario(27012) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 27012, r0

; endereco temporario(27011) recebe o conteudo da variavel: bola(27988)
load r0, 27988
store 27011, r0

; endereco temporario(27010) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 27010, r0

; expressao_binaria: 27011 + 27010
load r0, 27011
load r1, 27010
add r2, r0, r1
store 27011, r2

; @(95) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 27010, r0
loadn r0, #'c'
store 27009, r0
loadn r0, #27011
store 27008, r0
loadn r0, #0
store 27007, r0
load r0, 27013
load r1, 27012
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #27010
call __printf__
; variavel: key alocada em 27006 com 1 bytes

; endereco temporario(27005) recebe o conteudo: '!'
loadn r0, #'!'
store 27005, r0

load r0, 27005
store 27006, r0
; variavel: step alocada em 27005 com 1 bytes

; endereco temporario(27004) recebe o conteudo: 0
loadn r0, #0
store 27004, r0

load r0, 27004
store 27005, r0
; @(101) INSTRUCAO - while
__loop_label_begin14__:
; endereco temporario(27004) recebe o conteudo da variavel: key(27006)
load r0, 27006
store 27004, r0

; endereco temporario(27003) recebe o conteudo: 'q'
loadn r0, #'q'
store 27003, r0

; @(101) INSTRUCAO - relacao_binaria
load r0, 27004
load r1, 27003
loadn r2, #27004
call __not_equal_routine__
load r0, 27004
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label14__
jmp __loop_label14__
__loop_label14__:
; endereco temporario(27003) recebe o conteudo da variavel: key(27006)
load r0, 27006
store 27003, r0

inchar r0
store 27002, r0
; assignment salvando no endereco apontado por: endereco temporario(27003) o conteudo de endereco temporario(27002)
load r0, 27002
; valor da stack: 27001
store 27006, r0 ; variavel key recebe o conteudo de r0

; endereco temporario(27003) recebe o conteudo da variavel: key(27006)
load r0, 27006
store 27003, r0

; endereco temporario(27002) recebe o conteudo: 'x'
loadn r0, #'x'
store 27002, r0

; @(104) INSTRUCAO - relacao_binaria
load r0, 27003
load r1, 27002
loadn r2, #27003
call __equal_routine__
; endereco temporario(27002) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 27002, r0

; endereco temporario(27001) recebe o conteudo: 0
loadn r0, #0
store 27001, r0

; @(104) INSTRUCAO - relacao_binaria
load r0, 27002
load r1, 27001
loadn r2, #27002
call __equal_routine__
; @(104) INSTRUCAO - relacao_binaria
load r0, 27003
load r1, 27002
loadn r2, #27003
call __and_routine__
load r0, 27003
loadn r1, #0
cmp r0, r1
jeq __exit_if_label6__
jmp __if_label6__
__if_label6__:
; endereco temporario(27003) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 27003, r0

; endereco temporario(27002) recebe o conteudo: 3
loadn r0, #3
store 27002, r0

; assignment salvando no endereco apontado por: endereco temporario(27003) o conteudo de endereco temporario(27002)
load r0, 27002
; valor da stack: 27001
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label6__:
; endereco temporario(27003) recebe o conteudo da variavel: key(27006)
load r0, 27006
store 27003, r0

; endereco temporario(27002) recebe o conteudo: 'd'
loadn r0, #'d'
store 27002, r0

; @(108) INSTRUCAO - relacao_binaria
load r0, 27003
load r1, 27002
loadn r2, #27003
call __equal_routine__
; endereco temporario(27002) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 27002, r0

; endereco temporario(27001) recebe o conteudo: 25
loadn r0, #25
store 27001, r0

; @(108) INSTRUCAO - relacao_binaria
load r0, 27002
load r1, 27001
loadn r2, #27002
call __less_routine__
; @(108) INSTRUCAO - relacao_binaria
load r0, 27003
load r1, 27002
loadn r2, #27003
call __and_routine__
load r0, 27003
loadn r1, #0
cmp r0, r1
jeq __exit_if_label7__
jmp __if_label7__
__if_label7__:
; endereco temporario(27003) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 27003, r0

load r0, 27003
inc r0
store 27003, r0
store 27987, r0
; endereco temporario(27002) recebe o conteudo: 28
loadn r0, #28
store 27002, r0

; endereco temporario(27001) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 27001, r0

; endereco temporario(27000) recebe o conteudo: 1
loadn r0, #1
store 27000, r0

; expressao_binaria: 27001 - 27000
load r0, 27001
load r1, 27000
sub r2, r0, r1
store 27001, r2

; endereco temporario(27000) recebe o conteudo: ' '
loadn r0, #' '
store 27000, r0

; @(110) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26999, r0
loadn r0, #'c'
store 26998, r0
loadn r0, #27000
store 26997, r0
loadn r0, #0
store 26996, r0
load r0, 27002
load r1, 27001
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26999
call __printf__
__exit_if_label7__:
; endereco temporario(26995) recebe o conteudo da variavel: key(27006)
load r0, 27006
store 26995, r0

; endereco temporario(26994) recebe o conteudo: 'a'
loadn r0, #'a'
store 26994, r0

; @(113) INSTRUCAO - relacao_binaria
load r0, 26995
load r1, 26994
loadn r2, #26995
call __equal_routine__
; endereco temporario(26994) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26994, r0

; endereco temporario(26993) recebe o conteudo: 1
loadn r0, #1
store 26993, r0

; @(113) INSTRUCAO - relacao_binaria
load r0, 26994
load r1, 26993
loadn r2, #26994
call __more_routine__
; @(113) INSTRUCAO - relacao_binaria
load r0, 26995
load r1, 26994
loadn r2, #26995
call __and_routine__
load r0, 26995
loadn r1, #0
cmp r0, r1
jeq __exit_if_label8__
jmp __if_label8__
__if_label8__:
; endereco temporario(26995) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26995, r0

load r0, 26995
dec r0
store 26995, r0
store 27987, r0
; endereco temporario(26994) recebe o conteudo: 28
loadn r0, #28
store 26994, r0

; endereco temporario(26993) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26993, r0

; endereco temporario(26992) recebe o conteudo: 4
loadn r0, #4
store 26992, r0

; expressao_binaria: 26993 + 26992
load r0, 26993
load r1, 26992
add r2, r0, r1
store 26993, r2

; endereco temporario(26992) recebe o conteudo: ' '
loadn r0, #' '
store 26992, r0

; @(115) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26991, r0
loadn r0, #'c'
store 26990, r0
loadn r0, #26992
store 26989, r0
loadn r0, #0
store 26988, r0
load r0, 26994
load r1, 26993
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26991
call __printf__
__exit_if_label8__:
; endereco temporario(26987) recebe o conteudo: 28
loadn r0, #28
store 26987, r0

; endereco temporario(26986) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26986, r0

; endereco temporario(26985) recebe o conteudo da variavel: tabua_direita(27990)
load r0, 27990
store 26985, r0

; endereco temporario(26984) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 26984, r0

; expressao_binaria: 26985 + 26984
load r0, 26985
load r1, 26984
add r2, r0, r1
store 26985, r2

; @(118) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26984, r0
loadn r0, #'c'
store 26983, r0
loadn r0, #26985
store 26982, r0
loadn r0, #0
store 26981, r0
load r0, 26987
load r1, 26986
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26984
call __printf__
;----- begin for -----
; endereco temporario(26980) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 26980, r0

; endereco temporario(26979) recebe o conteudo: 1
loadn r0, #1
store 26979, r0

; assignment salvando no endereco apontado por: endereco temporario(26980) o conteudo de endereco temporario(26979)
load r0, 26979
; valor da stack: 26978
store 27982, r0 ; variavel i recebe o conteudo de r0

__loop_label_begin15__:
; endereco temporario(26980) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 26980, r0

; endereco temporario(26979) recebe o conteudo: 3
loadn r0, #3
store 26979, r0

; @(120) INSTRUCAO - relacao_binaria
load r0, 26980
load r1, 26979
loadn r2, #26980
call __less_routine__
load r0, 26980
loadn r1, #0
cmp r0, r1
jeq __exit_loop_label15__
jmp __loop_label3_begin15__
__loop_label2_begin15__:
; endereco temporario(26980) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 26980, r0

load r0, 26980
inc r0
store 26980, r0
store 27982, r0
jmp __loop_label_begin15__
__loop_label3_begin15__:
; endereco temporario(26979) recebe o conteudo: 28
loadn r0, #28
store 26979, r0

; endereco temporario(26978) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26978, r0

; endereco temporario(26977) recebe o conteudo da variavel: i(27982)
load r0, 27982
store 26977, r0

; expressao_binaria: 26978 + 26977
load r0, 26978
load r1, 26977
add r2, r0, r1
store 26978, r2

; endereco temporario(26977) recebe o conteudo da variavel: bloco(27989)
load r0, 27989
store 26977, r0

; endereco temporario(26976) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 26976, r0

; expressao_binaria: 26977 + 26976
load r0, 26977
load r1, 26976
add r2, r0, r1
store 26977, r2

; @(121) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26976, r0
loadn r0, #'c'
store 26975, r0
loadn r0, #26977
store 26974, r0
loadn r0, #0
store 26973, r0
load r0, 26979
load r1, 26978
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26976
call __printf__
jmp __loop_label2_begin15__
__exit_loop_label15__:
; ----- end for -----
; endereco temporario(26972) recebe o conteudo: 28
loadn r0, #28
store 26972, r0

; endereco temporario(26971) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26971, r0

; endereco temporario(26970) recebe o conteudo: 3
loadn r0, #3
store 26970, r0

; expressao_binaria: 26971 + 26970
load r0, 26971
load r1, 26970
add r2, r0, r1
store 26971, r2

; endereco temporario(26970) recebe o conteudo da variavel: tabua_esquerda(27991)
load r0, 27991
store 26970, r0

; endereco temporario(26969) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 26969, r0

; expressao_binaria: 26970 + 26969
load r0, 26970
load r1, 26969
add r2, r0, r1
store 26970, r2

; @(124) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26969, r0
loadn r0, #'c'
store 26968, r0
loadn r0, #26970
store 26967, r0
loadn r0, #0
store 26966, r0
load r0, 26972
load r1, 26971
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26969
call __printf__
; endereco temporario(26965) recebe o conteudo da variavel: step(27005)
load r0, 27005
store 26965, r0

; endereco temporario(26964) recebe o conteudo da variavel: bola_delay(27986)
load r0, 27986
store 26964, r0

; @(127) INSTRUCAO - relacao_binaria
load r0, 26965
load r1, 26964
loadn r2, #26965
call __equal_routine__
load r0, 26965
loadn r1, #0
cmp r0, r1
jeq __exit_if_label9__
jmp __if_label9__
__if_label9__:
; endereco temporario(26965) recebe o conteudo da variavel: step(27005)
load r0, 27005
store 26965, r0

; endereco temporario(26964) recebe o conteudo: 0
loadn r0, #0
store 26964, r0

; assignment salvando no endereco apontado por: endereco temporario(26965) o conteudo de endereco temporario(26964)
load r0, 26964
; valor da stack: 26963
store 27005, r0 ; variavel step recebe o conteudo de r0

; endereco temporario(26965) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26965, r0

; endereco temporario(26964) recebe o conteudo: 1
loadn r0, #1
store 26964, r0

; @(131) INSTRUCAO - relacao_binaria
load r0, 26965
load r1, 26964
loadn r2, #26965
call __equal_routine__
load r0, 26965
loadn r1, #0
cmp r0, r1
jeq __exit_if_label10__
jmp __if_label10__
__if_label10__:
; endereco temporario(26965) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26965, r0

; endereco temporario(26964) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26964, r0

; endereco temporario(26963) recebe o conteudo: ' '
loadn r0, #' '
store 26963, r0

; @(132) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26962, r0
loadn r0, #'c'
store 26961, r0
loadn r0, #26963
store 26960, r0
loadn r0, #0
store 26959, r0
load r0, 26965
load r1, 26964
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26962
call __printf__
; endereco temporario(26958) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26958, r0

load r0, 26958
dec r0
store 26958, r0
store 27984, r0
; endereco temporario(26957) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26957, r0

load r0, 26957
dec r0
store 26957, r0
store 27985, r0
; endereco temporario(26956) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26956, r0

; endereco temporario(26955) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26955, r0

; endereco temporario(26954) recebe o conteudo da variavel: bola(27988)
load r0, 27988
store 26954, r0

; endereco temporario(26953) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 26953, r0

; expressao_binaria: 26954 + 26953
load r0, 26954
load r1, 26953
add r2, r0, r1
store 26954, r2

; @(137) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26953, r0
loadn r0, #'c'
store 26952, r0
loadn r0, #26954
store 26951, r0
loadn r0, #0
store 26950, r0
load r0, 26956
load r1, 26955
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26953
call __printf__
__exit_if_label10__:
; endereco temporario(26949) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26949, r0

; endereco temporario(26948) recebe o conteudo: 2
loadn r0, #2
store 26948, r0

; @(141) INSTRUCAO - relacao_binaria
load r0, 26949
load r1, 26948
loadn r2, #26949
call __equal_routine__
load r0, 26949
loadn r1, #0
cmp r0, r1
jeq __exit_if_label11__
jmp __if_label11__
__if_label11__:
; endereco temporario(26949) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26949, r0

; endereco temporario(26948) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26948, r0

; endereco temporario(26947) recebe o conteudo: ' '
loadn r0, #' '
store 26947, r0

; @(142) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26946, r0
loadn r0, #'c'
store 26945, r0
loadn r0, #26947
store 26944, r0
loadn r0, #0
store 26943, r0
load r0, 26949
load r1, 26948
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26946
call __printf__
; endereco temporario(26942) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26942, r0

load r0, 26942
dec r0
store 26942, r0
store 27984, r0
; endereco temporario(26941) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26941, r0

load r0, 26941
inc r0
store 26941, r0
store 27985, r0
; endereco temporario(26940) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26940, r0

; endereco temporario(26939) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26939, r0

; endereco temporario(26938) recebe o conteudo da variavel: bola(27988)
load r0, 27988
store 26938, r0

; endereco temporario(26937) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 26937, r0

; expressao_binaria: 26938 + 26937
load r0, 26938
load r1, 26937
add r2, r0, r1
store 26938, r2

; @(147) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26937, r0
loadn r0, #'c'
store 26936, r0
loadn r0, #26938
store 26935, r0
loadn r0, #0
store 26934, r0
load r0, 26940
load r1, 26939
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26937
call __printf__
__exit_if_label11__:
; endereco temporario(26933) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26933, r0

; endereco temporario(26932) recebe o conteudo: 3
loadn r0, #3
store 26932, r0

; @(150) INSTRUCAO - relacao_binaria
load r0, 26933
load r1, 26932
loadn r2, #26933
call __equal_routine__
load r0, 26933
loadn r1, #0
cmp r0, r1
jeq __exit_if_label12__
jmp __if_label12__
__if_label12__:
; endereco temporario(26933) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26933, r0

; endereco temporario(26932) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26932, r0

; endereco temporario(26931) recebe o conteudo: ' '
loadn r0, #' '
store 26931, r0

; @(151) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26930, r0
loadn r0, #'c'
store 26929, r0
loadn r0, #26931
store 26928, r0
loadn r0, #0
store 26927, r0
load r0, 26933
load r1, 26932
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26930
call __printf__
; endereco temporario(26926) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26926, r0

load r0, 26926
inc r0
store 26926, r0
store 27984, r0
; endereco temporario(26925) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26925, r0

load r0, 26925
dec r0
store 26925, r0
store 27985, r0
; endereco temporario(26924) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26924, r0

; endereco temporario(26923) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26923, r0

; endereco temporario(26922) recebe o conteudo da variavel: bola(27988)
load r0, 27988
store 26922, r0

; endereco temporario(26921) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 26921, r0

; expressao_binaria: 26922 + 26921
load r0, 26922
load r1, 26921
add r2, r0, r1
store 26922, r2

; @(156) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26921, r0
loadn r0, #'c'
store 26920, r0
loadn r0, #26922
store 26919, r0
loadn r0, #0
store 26918, r0
load r0, 26924
load r1, 26923
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26921
call __printf__
__exit_if_label12__:
; endereco temporario(26917) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26917, r0

; endereco temporario(26916) recebe o conteudo: 4
loadn r0, #4
store 26916, r0

; @(159) INSTRUCAO - relacao_binaria
load r0, 26917
load r1, 26916
loadn r2, #26917
call __equal_routine__
load r0, 26917
loadn r1, #0
cmp r0, r1
jeq __exit_if_label13__
jmp __if_label13__
__if_label13__:
; endereco temporario(26917) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26917, r0

; endereco temporario(26916) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26916, r0

; endereco temporario(26915) recebe o conteudo: ' '
loadn r0, #' '
store 26915, r0

; @(160) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26914, r0
loadn r0, #'c'
store 26913, r0
loadn r0, #26915
store 26912, r0
loadn r0, #0
store 26911, r0
load r0, 26917
load r1, 26916
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26914
call __printf__
; endereco temporario(26910) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26910, r0

load r0, 26910
inc r0
store 26910, r0
store 27984, r0
; endereco temporario(26909) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26909, r0

load r0, 26909
inc r0
store 26909, r0
store 27985, r0
; endereco temporario(26908) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26908, r0

; endereco temporario(26907) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26907, r0

; endereco temporario(26906) recebe o conteudo da variavel: bola(27988)
load r0, 27988
store 26906, r0

; endereco temporario(26905) recebe o conteudo da variavel: cor_vermelho(27993)
load r0, 27993
store 26905, r0

; expressao_binaria: 26906 + 26905
load r0, 26906
load r1, 26905
add r2, r0, r1
store 26906, r2

; @(165) INSTRUCAO - ; printf(%c);
loadn r0, #'%'
store 26905, r0
loadn r0, #'c'
store 26904, r0
loadn r0, #26906
store 26903, r0
loadn r0, #0
store 26902, r0
load r0, 26908
load r1, 26907
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26905
call __printf__
__exit_if_label13__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 28
loadn r0, #28
store 26900, r0

; @(168) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label14__
jmp __if_label14__
__if_label14__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 10
loadn r0, #10
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27985, r0 ; variavel bola_x recebe o conteudo de r0

; endereco temporario(26901) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 20
loadn r0, #20
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27984, r0 ; variavel bola_y recebe o conteudo de r0

; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 0
loadn r0, #0
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label14__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 2
loadn r0, #2
store 26900, r0

; @(176) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __less_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label15__
jmp __if_label15__
__if_label15__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 3
loadn r0, #3
store 26900, r0

; @(178) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label16__
jmp __if_label16__
__if_label16__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 4
loadn r0, #4
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label16__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 1
loadn r0, #1
store 26900, r0

; @(182) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label17__
jmp __if_label17__
__if_label17__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 2
loadn r0, #2
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label17__:
__exit_if_label15__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 27
loadn r0, #27
store 26900, r0

; @(188) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __more_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label18__
jmp __if_label18__
__if_label18__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 2
loadn r0, #2
store 26900, r0

; @(189) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label19__
jmp __if_label19__
__if_label19__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 1
loadn r0, #1
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label19__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 4
loadn r0, #4
store 26900, r0

; @(193) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label20__
jmp __if_label20__
__if_label20__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 3
loadn r0, #3
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label20__:
__exit_if_label18__:
; endereco temporario(26901) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 1
loadn r0, #1
store 26900, r0

; expressao_binaria: 26901 - 26900
load r0, 26901
load r1, 26900
sub r2, r0, r1
store 26901, r2

; endereco temporario(26900) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26900, r0

; @(198) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __less_routine__
; endereco temporario(26900) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26900, r0

; endereco temporario(26899) recebe o conteudo da variavel: pos_x(27987)
load r0, 27987
store 26899, r0

; endereco temporario(26898) recebe o conteudo: 4
loadn r0, #4
store 26898, r0

; expressao_binaria: 26899 + 26898
load r0, 26899
load r1, 26898
add r2, r0, r1
store 26899, r2

; @(198) INSTRUCAO - relacao_binaria
load r0, 26900
load r1, 26899
loadn r2, #26900
call __less_routine__
; @(198) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __and_routine__
; endereco temporario(26900) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26900, r0

; endereco temporario(26899) recebe o conteudo: 27
loadn r0, #27
store 26899, r0

; @(198) INSTRUCAO - relacao_binaria
load r0, 26900
load r1, 26899
loadn r2, #26900
call __equal_routine__
; @(198) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __and_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label21__
jmp __if_label21__
__if_label21__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 3
loadn r0, #3
store 26900, r0

; @(199) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label22__
jmp __if_label22__
__if_label22__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 1
loadn r0, #1
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label22__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 4
loadn r0, #4
store 26900, r0

; @(203) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label23__
jmp __if_label23__
__if_label23__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 2
loadn r0, #2
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label23__:
__exit_if_label21__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 4
loadn r0, #4
store 26900, r0

; @(208) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __less_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label24__
jmp __if_label24__
__if_label24__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 1
loadn r0, #1
store 26900, r0

; @(209) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label25__
jmp __if_label25__
__if_label25__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 3
loadn r0, #3
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label25__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 2
loadn r0, #2
store 26900, r0

; @(213) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label26__
jmp __if_label26__
__if_label26__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26901, r0

; endereco temporario(26900) recebe o conteudo: 4
loadn r0, #4
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label26__:
__exit_if_label24__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26901, r0

; endereco temporario(26900) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26900, r0

; endereco temporario(26899) recebe o conteudo da variavel: mapa
load r0, 26900
load r1, 26901
load r2, 27980
sub r2, r2, r1
loadi r1, r2
sub r7, r1, r0
loadi r1, r7
store 26901, r1
; matriz, valor da stack 26901
; endereco temporario(26900) recebe o conteudo da variavel: bloco(27989)
load r0, 27989
store 26900, r0

; @(218) INSTRUCAO - relacao_binaria
load r0, 26901
load r1, 26900
loadn r2, #26901
call __equal_routine__
load r0, 26901
loadn r1, #0
cmp r0, r1
jeq __exit_if_label27__
jmp __if_label27__
__if_label27__:
; endereco temporario(26901) recebe o conteudo da variavel: bola_x(27985)
load r0, 27985
store 26901, r0

; endereco temporario(26900) recebe o conteudo da variavel: bola_y(27984)
load r0, 27984
store 26900, r0

; endereco temporario(26899) recebe o conteudo da variavel: mapa
load r0, 26900
load r1, 26901
load r2, 27980
sub r2, r2, r1
loadi r1, r2
sub r7, r1, r0
loadi r1, r7
store 26901, r1
; matriz, valor da stack 26901
; endereco temporario(26900) recebe o conteudo: ' '
loadn r0, #' '
store 26900, r0

; assignment salvando no endereco apontado por: endereco temporario(26901) o conteudo de endereco temporario(26900)
load r0, 26900
; valor da stack: 26899
storei r7, r0 ; endereco apontado por r7 recebe o conteudo de r0

; endereco temporario(26901) recebe o conteudo da variavel: score(27028)
load r0, 27028
store 26901, r0

load r0, 26901
inc r0
store 26901, r0
store 27028, r0
; endereco temporario(26900) recebe o conteudo: 0
loadn r0, #0
store 26900, r0

; endereco temporario(26899) recebe o conteudo: 1
loadn r0, #1
store 26899, r0

; endereco temporario(26898) recebe o conteudo da variavel: score(27028)
load r0, 27028
store 26898, r0

; @(222) INSTRUCAO - ; printf(Score: %d);
loadn r0, #'S'
store 26897, r0
loadn r0, #'c'
store 26896, r0
loadn r0, #'o'
store 26895, r0
loadn r0, #'r'
store 26894, r0
loadn r0, #'e'
store 26893, r0
loadn r0, #':'
store 26892, r0
loadn r0, #' '
store 26891, r0
loadn r0, #'%'
store 26890, r0
loadn r0, #'d'
store 26889, r0
;comeco da string: 26897
;printf_args.size(): 1
; 26898
;j: 0
loadn r0, #26898
store 26888, r0
loadn r0, #0
store 26887, r0
load r0, 26900
load r1, 26899
loadn r2, #40
mul r0, r0, r2
add r0, r0, r1
loadn r2, #26897
call __printf__
; endereco temporario(26886) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26886, r0

; endereco temporario(26885) recebe o conteudo: 1
loadn r0, #1
store 26885, r0

; @(224) INSTRUCAO - relacao_binaria
load r0, 26886
load r1, 26885
loadn r2, #26886
call __equal_routine__
load r0, 26886
loadn r1, #0
cmp r0, r1
jeq __exit_if_label28__
jmp __if_label28__
__if_label28__:
; endereco temporario(26886) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26886, r0

; endereco temporario(26885) recebe o conteudo: 3
loadn r0, #3
store 26885, r0

; assignment salvando no endereco apontado por: endereco temporario(26886) o conteudo de endereco temporario(26885)
load r0, 26885
; valor da stack: 26884
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label28__:
; endereco temporario(26886) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26886, r0

; endereco temporario(26885) recebe o conteudo: 2
loadn r0, #2
store 26885, r0

; @(228) INSTRUCAO - relacao_binaria
load r0, 26886
load r1, 26885
loadn r2, #26886
call __equal_routine__
load r0, 26886
loadn r1, #0
cmp r0, r1
jeq __exit_if_label29__
jmp __if_label29__
__if_label29__:
; endereco temporario(26886) recebe o conteudo da variavel: bola_direcao(27983)
load r0, 27983
store 26886, r0

; endereco temporario(26885) recebe o conteudo: 4
loadn r0, #4
store 26885, r0

; assignment salvando no endereco apontado por: endereco temporario(26886) o conteudo de endereco temporario(26885)
load r0, 26885
; valor da stack: 26884
store 27983, r0 ; variavel bola_direcao recebe o conteudo de r0

__exit_if_label29__:
__exit_if_label27__:
__exit_if_label9__:
; endereco temporario(26886) recebe o conteudo da variavel: step(27005)
load r0, 27005
store 26886, r0

load r0, 26886
inc r0
store 26886, r0
store 27005, r0
jmp __loop_label_begin14__
__exit_loop_label14__:
; ----- end while -----
breakp
breakp
rts
;---------- END ----------
