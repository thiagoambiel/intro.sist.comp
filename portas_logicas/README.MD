# Portas Lógicas Criadas com [Falstad](https://www.falstad.com/circuit/)

Foi proposto durante as aulas a montagem das principais portas lógicas usadas na industria de microprocessadores através do simulador **Falstad**.

As seguintes portas lógicas são implementadas aqui:
- [Porta **NOT**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/blob/main/portas_logicas/PORTA_NOT.txt)
- [Porta **AND**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/blob/main/portas_logicas/Porta-AND.txt)
- [Porta **OR**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/blob/main/portas_logicas/Porta-OR.txt)
- [Porta **XOR**](https://gitlab.com/thiagoambiel/intro.sist.comp/-/blob/main/portas_logicas/Porta_XOR.txt)
- [Porta **XOR** (Com 3 Entradas)](https://gitlab.com/thiagoambiel/intro.sist.comp/-/blob/main/portas_logicas/Porta_XOR_3_Entradas.txt)
